import {firestore} from "firebase";
import Firestore = firestore.Firestore;

export interface Serializeable<S> {
    serialize(): Unserializeable<S>;
}

export interface Unserializeable<S> {
    unserialize(db: Firestore): Serializeable<S>;
}
