export class Pair<T, V> {
    public first: T;
    public second: V;

    constructor(first: T, second: V) {
        this.first = first;
        this.second = second;
    }
}
