import {firestore} from "firebase";
// noinspection TypeScriptPreferShortImport
import {Serializeable, Unserializeable} from "../serial/Serialize";
import DocumentReference = firestore.DocumentReference;
import Timestamp = firestore.Timestamp;
import {AbstractBus} from "./abstract/AbstractBus";
import {SerializedBus} from "./serialized/SerializedBus";

export class Bus extends AbstractBus implements Serializeable<AbstractBus> {
    public readonly route: DocumentReference;
    public readonly timetable: Map<DocumentReference, Timestamp>;
    public readonly lastUpdated: Timestamp;

    constructor(id: string, passengers: number | undefined, rate: number, progress: number | undefined,
                route: DocumentReference, timetable: Map<DocumentReference, Timestamp>, lastUpdated: Timestamp) {
        super(id, passengers, rate, progress);
        this.route = route;
        this.timetable = timetable;
        this.lastUpdated = lastUpdated;
    }

    public serialize(): Unserializeable<AbstractBus> {
        const route: string = this.route.path;
        const timetable: Map<string, number> = new Map<string, number>();
        [...this.timetable].forEach(([stop, time]) => {
            timetable.set(stop.path, time.toMillis()); // because mapping doesn't work apparently
        });
        const lastUpdated = this.lastUpdated.toMillis();
        return new SerializedBus(this.id, this.passengers, this.rate, this.progress, route, timetable, lastUpdated);
    }
}
