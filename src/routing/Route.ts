import {firestore} from "firebase";
import DocumentReference = firestore.DocumentReference;
import GeoPoint = firestore.GeoPoint;
import Timestamp = firestore.Timestamp;
// noinspection TypeScriptPreferShortImport
import {Pair} from "../misc/Pair";
// noinspection TypeScriptPreferShortImport
import {Serializeable, Unserializeable} from "../serial/Serialize";
// noinspection TypeScriptPreferShortImport
import {AbstractRoute} from "./abstract/AbstractRoute";
import {SerializedRoute} from "./serialized/SerializedRoute";

export class Route extends AbstractRoute implements Serializeable<AbstractRoute> {
    public readonly buses: DocumentReference[];
    public readonly stops: DocumentReference[];
    public readonly pattern: GeoPoint[];
    public readonly lastUpdated: Timestamp;

    constructor(id: string, name: string, buses: DocumentReference[], stops: DocumentReference[],
                pattern: GeoPoint[], lastUpdated: Timestamp) {
        super(id, name);
        this.buses = buses;
        this.stops = stops;
        this.pattern = pattern;
        this.lastUpdated = lastUpdated;
    }

    public serialize(): Unserializeable<AbstractRoute> {
        const buses: string[] = this.buses.map((bus) => bus.path);
        const stops: string[] = this.stops.map((stop) => stop.path);
        const pattern: Array<Pair<number, number>> = this.pattern
            .map((point) => new Pair(point.latitude, point.longitude));
        const lastUpdated: number = this.lastUpdated.toMillis();
        return new SerializedRoute(this.id, this.name, buses, stops, pattern, lastUpdated);
    }
}
