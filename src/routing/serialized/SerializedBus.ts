import {firestore} from "firebase";
// noinspection TypeScriptPreferShortImport
import {Serializeable, Unserializeable} from "../../serial/Serialize";
// noinspection TypeScriptPreferShortImport
import {AbstractBus} from "../abstract/AbstractBus";
import Firestore = firestore.Firestore;
import DocumentReference = firestore.DocumentReference;
import Timestamp = firestore.Timestamp;
import {Bus} from "../Bus";

export class SerializedBus extends AbstractBus implements Unserializeable<AbstractBus> {
    public readonly route: string;
    public readonly timetable: Map<string, number>;
    public readonly lastUpdated: number;

    constructor(id: string, passengers: number | undefined, rate: number, progress: number | undefined,
                route: string, timetable: Map<string, number>, lastUpdated: number) {
        super(id, passengers, rate, progress);
        this.route = route;
        this.timetable = timetable;
        this.lastUpdated = lastUpdated;
    }

    public unserialize(db: Firestore): Serializeable<AbstractBus> {
        const route: DocumentReference = db.doc(this.route);
        const timetable: Map<DocumentReference, Timestamp> = new Map<DocumentReference, Timestamp>();
        [...this.timetable].forEach(([stop, time]) => {
            timetable.set(db.doc(stop), Timestamp.fromMillis(time)); // because mapping doesn't work apparently
        });
        const lastUpdated = Timestamp.fromMillis(this.lastUpdated);
        return new Bus(this.id, this.passengers, this.rate, this.progress, route, timetable, lastUpdated);
    }
}
