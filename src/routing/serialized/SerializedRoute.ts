import {firestore} from "firebase";
import DocumentReference = firestore.DocumentReference;
import GeoPoint = firestore.GeoPoint;
import Timestamp = firestore.Timestamp;
import Firestore = firestore.Firestore;
// noinspection TypeScriptPreferShortImport
import {Pair} from "../../misc/Pair";
// noinspection TypeScriptPreferShortImport
import {Serializeable, Unserializeable} from "../../serial/Serialize";
// noinspection TypeScriptPreferShortImport
import {AbstractRoute} from "../abstract/AbstractRoute";
import {Route} from "../Route";

export class SerializedRoute extends AbstractRoute implements Unserializeable<AbstractRoute> {
    public readonly buses: string[];
    public readonly stops: string[];
    public readonly pattern: Array<Pair<number, number>>;
    public readonly lastUpdated: number;

    constructor(id: string, name: string, buses: string[], stops: string[],
                pattern: Array<Pair<number, number>>, lastUpdated: number) {
        super(id, name);
        this.buses = buses;
        this.stops = stops;
        this.pattern = pattern;
        this.lastUpdated = lastUpdated;
    }

    public unserialize(db: Firestore): Serializeable<AbstractRoute> {
        const buses: DocumentReference[] = this.buses.map((bus) => db.doc(bus));
        const stops: DocumentReference[] = this.stops.map((stop) => db.doc(stop));
        const pattern: GeoPoint[] = this.pattern.map((pair) => new GeoPoint(pair.first, pair.second));
        const lastUpdated: Timestamp = Timestamp.fromMillis(this.lastUpdated);
        return new Route(this.id, this.name, buses, stops, pattern, lastUpdated);
    }
}
