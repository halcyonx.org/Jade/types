import {firestore} from "firebase";
// noinspection TypeScriptPreferShortImport
import {Serializeable, Unserializeable} from "../../serial/Serialize";
// noinspection TypeScriptPreferShortImport
import {AbstractStop} from "../abstract/AbstractStop";
import Firestore = firestore.Firestore;
import DocumentReference = firestore.DocumentReference;
import Timestamp = firestore.Timestamp;
import {Stop} from "../Stop";

export class SerializedStop extends AbstractStop implements Unserializeable<AbstractStop> {
    public readonly routes: string[];
    public readonly timetable: number[];

    constructor(id: string, name: string, probabalistic: boolean, routes: string[], timetable: number[]) {
        super(id, name, probabalistic);
        this.routes = routes;
        this.timetable = timetable;
    }

    public unserialize(db: Firestore): Serializeable<AbstractStop> {
        const routes: DocumentReference[] = this.routes.map((route) => db.doc(route));
        const timetable: Timestamp[] = this.timetable.map((time) => Timestamp.fromMillis(time));
        return new Stop(this.id, this.name, this.probabalistic, routes, timetable);
    }
}
