import {DocumentReference, Timestamp} from "@firebase/firestore-types";
// noinspection TypeScriptPreferShortImport
import {Serializeable, Unserializeable} from "../serial/Serialize";
import {AbstractStop} from "./abstract/AbstractStop";
import {SerializedStop} from "./serialized/SerializedStop";

export class Stop extends AbstractStop implements Serializeable<AbstractStop> {
    public readonly routes: DocumentReference[];
    public readonly timetable: Timestamp[];

    constructor(id: string, name: string, probabalistic: boolean, routes: DocumentReference[], timetable: Timestamp[]) {
        super(id, name, probabalistic);
        this.routes = routes;
        this.timetable = timetable;
    }

    public serialize(): Unserializeable<AbstractStop> {
        const routes: string[] = this.routes.map((route) => route.path);
        const timetable: number[] = this.timetable.map((time) => time.toMillis());
        return new SerializedStop(this.id, this.name, this.probabalistic, routes, timetable);
    }
}
