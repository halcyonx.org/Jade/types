export class AbstractStop {
    public readonly id: string;
    public readonly name: string;
    public readonly probabalistic: boolean;

    constructor(id: string, name: string, probabalistic: boolean) {
        this.id = id;
        this.name = name;
        this.probabalistic = probabalistic;
    }
}
