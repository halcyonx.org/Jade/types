export class AbstractBus {
    public readonly id: string;
    public readonly passengers: number | undefined;
    public readonly rate: number;
    public readonly progress: number | undefined;

    constructor(id: string, passengers: number | undefined, rate: number, progress: number | undefined) {
        this.id = id;
        this.passengers = passengers;
        this.rate = rate;
        this.progress = progress;
    }
}
