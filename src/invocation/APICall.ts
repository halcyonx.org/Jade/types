import {Call} from "./Call";

export class APICall extends Call {
    public readonly domain: string;

    constructor(domain: string, target: string, requested: string[]) {
        super(target, requested);
        this.domain = domain;
    }
}
