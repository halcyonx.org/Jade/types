export class Call {
    public readonly target: string;
    public readonly requested: string[];

    constructor(target: string, requested: string[]) {
        this.target = target;
        this.requested = requested;
    }
}
