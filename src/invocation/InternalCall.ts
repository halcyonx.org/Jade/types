import {Call} from "./Call";

export class InternalCall extends Call {
    public readonly resource: string;

    constructor(resource: string, target: string, requested: string[]) {
        super(target, requested);
        this.resource = resource;
    }
}
